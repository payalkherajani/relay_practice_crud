/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type DesignationList_designationListProps = ReadonlyArray<{
    readonly id: string;
    readonly name: string;
    readonly description: string;
    readonly " $refType": "DesignationList_designationListProps";
}>;
export type DesignationList_designationListProps$data = DesignationList_designationListProps;
export type DesignationList_designationListProps$key = ReadonlyArray<{
    readonly " $data"?: DesignationList_designationListProps$data;
    readonly " $fragmentRefs": FragmentRefs<"DesignationList_designationListProps">;
}>;



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": {
    "plural": true
  },
  "name": "DesignationList_designationListProps",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    }
  ],
  "type": "Designation",
  "abstractKey": null
};
(node as any).hash = '575ca75d234b8a47c9df66f88372a663';
export default node;
