/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type DesignationUpdate_designationUpdateProps = ReadonlyArray<{
    readonly id: string;
    readonly name: string;
    readonly description: string;
    readonly roleIds: ReadonlyArray<string>;
    readonly " $refType": "DesignationUpdate_designationUpdateProps";
}>;
export type DesignationUpdate_designationUpdateProps$data = DesignationUpdate_designationUpdateProps;
export type DesignationUpdate_designationUpdateProps$key = ReadonlyArray<{
    readonly " $data"?: DesignationUpdate_designationUpdateProps$data;
    readonly " $fragmentRefs": FragmentRefs<"DesignationUpdate_designationUpdateProps">;
}>;



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": {
    "plural": true
  },
  "name": "DesignationUpdate_designationUpdateProps",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "roleIds",
      "storageKey": null
    }
  ],
  "type": "Designation",
  "abstractKey": null
};
(node as any).hash = '0737e6c02e6019ba52ca5f57f6815861';
export default node;
