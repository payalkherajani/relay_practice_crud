/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type DesignationMaster_designationsprops = {
    readonly designations: {
        readonly designation: ReadonlyArray<{
            readonly " $fragmentRefs": FragmentRefs<"DesignationList_designationListProps" | "DesignationUpdate_designationUpdateProps">;
        }>;
    };
    readonly " $refType": "DesignationMaster_designationsprops";
};
export type DesignationMaster_designationsprops$data = DesignationMaster_designationsprops;
export type DesignationMaster_designationsprops$key = {
    readonly " $data"?: DesignationMaster_designationsprops$data;
    readonly " $fragmentRefs": FragmentRefs<"DesignationMaster_designationsprops">;
};



const node: ReaderFragment = {
  "argumentDefinitions": [
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "parent"
    }
  ],
  "kind": "Fragment",
  "metadata": null,
  "name": "DesignationMaster_designationsprops",
  "selections": [
    {
      "alias": null,
      "args": [
        {
          "kind": "Variable",
          "name": "parent",
          "variableName": "parent"
        }
      ],
      "concreteType": "ListDesignationResponse",
      "kind": "LinkedField",
      "name": "designations",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "Designation",
          "kind": "LinkedField",
          "name": "designation",
          "plural": true,
          "selections": [
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "DesignationList_designationListProps"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "DesignationUpdate_designationUpdateProps"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Query",
  "abstractKey": null
};
(node as any).hash = '2ad6dffaf62a1974e39a3e4c09481a67';
export default node;
