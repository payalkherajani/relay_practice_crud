/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type DesignationPageQueryVariables = {
    parent?: string | null;
};
export type DesignationPageQueryResponse = {
    readonly " $fragmentRefs": FragmentRefs<"DesignationMaster_designationsprops">;
};
export type DesignationPageQuery = {
    readonly response: DesignationPageQueryResponse;
    readonly variables: DesignationPageQueryVariables;
};



/*
query DesignationPageQuery(
  $parent: String
) {
  ...DesignationMaster_designationsprops_2XQG37
}

fragment DesignationList_designationListProps on Designation {
  id
  name
  description
}

fragment DesignationMaster_designationsprops_2XQG37 on Query {
  designations(parent: $parent) {
    designation {
      ...DesignationList_designationListProps
      ...DesignationUpdate_designationUpdateProps
      id
    }
  }
}

fragment DesignationUpdate_designationUpdateProps on Designation {
  id
  name
  description
  roleIds
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "parent"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "parent",
    "variableName": "parent"
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "DesignationPageQuery",
    "selections": [
      {
        "args": (v1/*: any*/),
        "kind": "FragmentSpread",
        "name": "DesignationMaster_designationsprops"
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "DesignationPageQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "ListDesignationResponse",
        "kind": "LinkedField",
        "name": "designations",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Designation",
            "kind": "LinkedField",
            "name": "designation",
            "plural": true,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "roleIds",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "3450b77ed9a29f4d6f57b9cca81927a0",
    "id": null,
    "metadata": {},
    "name": "DesignationPageQuery",
    "operationKind": "query",
    "text": "query DesignationPageQuery(\n  $parent: String\n) {\n  ...DesignationMaster_designationsprops_2XQG37\n}\n\nfragment DesignationList_designationListProps on Designation {\n  id\n  name\n  description\n}\n\nfragment DesignationMaster_designationsprops_2XQG37 on Query {\n  designations(parent: $parent) {\n    designation {\n      ...DesignationList_designationListProps\n      ...DesignationUpdate_designationUpdateProps\n      id\n    }\n  }\n}\n\nfragment DesignationUpdate_designationUpdateProps on Designation {\n  id\n  name\n  description\n  roleIds\n}\n"
  }
};
})();
(node as any).hash = '1ad66fdc7828c28195f33bb6a192e055';
export default node;
