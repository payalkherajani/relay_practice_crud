import { Trans } from '@lingui/macro'
import { Mutable, useAlert } from '@saastack/core'
import { FormContainerProps } from '@saastack/layouts/types'
import { FormContainer } from '@saastack/layouts/containers'
import React, { useState, useEffect } from 'react'
import { createFragmentContainer } from 'react-relay'
import { graphql } from 'relay-runtime'
import { useRelayEnvironment } from 'react-relay/hooks'
import DesignationAddValidations from '../utils/DesignationAddValidations'
import { DesignationInput } from '../__generated__/CreateDesignationMutation.graphql'
import { DesignationUpdate_designationUpdateProps } from '../__generated__/DesignationUpdate_designationUpdateProps.graphql'
import DesignationUpdateFormComponent from '../forms/DesignationUpdateFormComponent'
import UpdateDesignationMutation from '../mutations/UpdateDesignationMutation'

interface Props extends Omit<FormContainerProps, 'formId'>{
  variables: { parent: string },
  id: string,
  onClose: () => void,
  onSuccess: () => void
  designationUpdateProps: DesignationUpdate_designationUpdateProps
}


const formId = 'designation-update-form'
// { designationsUpdateProps, id, onClose,onSuccess, ...props}

const DesignationUpdate: React.FC<Props> = (props) => {
  
    console.log(props,"props  in designation")
    const environment = useRelayEnvironment()

    const showAlert = useAlert()

    const designation = props.designationUpdateProps.find((i: any) => i.id === props.id)!
    
    const [loading,setLoading] = useState(false)
    
    useEffect(() => {
      if(!designation){
          props.onClose()
      }
    },[designation])
    
    const handleSubmit = (designation: DesignationInput) => {
        console.log(designation,"update designation values")
        setLoading(true)
        UpdateDesignationMutation.commit(environment,designation,['name','description'], {
            onSuccess: handleSuccess,
            onError,
        })
    }

    const onError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error',
        })
    }
    const handleSuccess = (response: DesignationInput) => {
        setLoading(false)
        showAlert(<Trans>Designation updated successfully!</Trans>, {
            variant: 'info',
        })
        props.onSuccess()
    }

    const initialValuesToSend = designation as Mutable<DesignationUpdate_designationUpdateProps[0]>
    if (!designation) {
        return null
    }
    return (
        <FormContainer
          open
          onClose={props.onClose}
          header={<Trans>Update Designation</Trans>}
          formId={formId}
          loading={loading}
          props={props}
        >
           <DesignationUpdateFormComponent 
             update={true}
             onSubmit={handleSubmit}
             id={formId}
             initialValues={initialValuesToSend}
             validationSchema={DesignationAddValidations}
           />
        </FormContainer>
    )


}

export default createFragmentContainer(DesignationUpdate, {
    designationUpdateProps: graphql`
         fragment DesignationUpdate_designationUpdateProps on Designation @relay(plural: true) {
            id
            name
            description
            roleIds
         }
`
})
