import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { DesignationList_designationListProps } from '../__generated__/DesignationList_designationListProps.graphql'
import {
    IconButton,
    List,
    ListItem,
    ListItemAvatar,
    ListItemSecondaryAction,
    ListItemText,
    Tooltip,
} from '@material-ui/core'
import { Avatar } from '@saastack/components'
import { DeleteOutlined } from '@material-ui/icons'
import { Trans } from '@lingui/macro'
import { makeStyles } from '@material-ui/styles'

interface Props {
    designationListProps: DesignationList_designationListProps
    onHandleClick: (id: string, action: 'UPDATE' | 'DELETE') => void
}

const useStyles = makeStyles({
    list: {
        '& .MuiListItemSecondaryAction-root': {
            visibility: 'hidden',
        },
        '& li:hover': {
            '& .MuiListItemSecondaryAction-root': {
                visibility: 'visible',
            },
        },
    },
})

const DesignationList: React.FC<Props> = (props) => {
    
    const classes = useStyles()
    // console.log(props,"designation List Props")

    return (
        <List className={classes.list}>
            {props.designationListProps.map((d) => (
                <ListItem key={d.id} divider onClick={() => props.onHandleClick(d.id,'UPDATE')}>
                    <ListItemAvatar>
                        <Avatar title={d.name} />
                    </ListItemAvatar>
                    <ListItemText primary={d.name} secondary={d.description} />
                    <ListItemSecondaryAction>
                        <Tooltip title={<Trans>Delete {d.name}</Trans>}>
                            <IconButton onClick={() => props.onHandleClick(d.id,'DELETE')}>
                                <DeleteOutlined />
                            </IconButton>
                        </Tooltip>
                    </ListItemSecondaryAction>

                </ListItem>
            ))}
        </List>
    )
}

export default createFragmentContainer(DesignationList, {
    designationListProps: graphql`
        fragment DesignationList_designationListProps on Designation @relay(plural: true) {
            id
            name
            description
        }
    `,
})
