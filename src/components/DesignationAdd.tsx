import React, { useState } from 'react'
import { FormContainer } from '@saastack/layouts/containers'
import { FormContainerProps } from '@saastack/layouts/types'
import { Trans } from '@lingui/macro'
import DesignationAddFormComponent from '../forms/DesignationAddFormComponent'
import DesignationAddInitialValues from '../utils/DesignationAddInitialValues'
import DesignationAddValidations from '../utils/DesignationAddValidations'
import CreateDesignationMutation from '../mutations/CreateDesignationMutation'
import { useRelayEnvironment } from 'react-relay/hooks'
import { DesignationInput } from '../__generated__/CreateDesignationMutation.graphql'
import { useAlert } from '@saastack/core'
import { Variables } from 'react-relay'

interface Props extends Omit<FormContainerProps, 'formId'> {
  variables: Variables
  onClose: () => void
  onSuccess: () => void
}



const formId = 'designation-add-form'

const DesignationAdd: React.FC<Props> = (props) => {
    
   

    const [loading,setLoading] = useState(false)
    const initialValuesToSend = { ...DesignationAddInitialValues }
    const environment = useRelayEnvironment()
    const showAlert = useAlert()

    const onError = (e: string) => {
        console.log(e,"e in onError designation-add")
        setLoading(false)
        showAlert(e, {
            variant: 'error',
        })
    }

    const handleSuccess = (response: DesignationInput) => {
        setLoading(false)
        showAlert(<Trans>Designation added successfully!</Trans>, {
            variant: 'info',
        })
        props.onSuccess()
    }

    const handleSubmit = (values: DesignationInput) => {
     console.log(values,"handle submit in designation add")
     const designation = {
         ...values
     }
     CreateDesignationMutation.commit(environment, props.variables, designation, {
        onSuccess: handleSuccess,
        onError
     })
    }


   
    return (
        <FormContainer
         open
         onClose={props.onClose}
         header={<Trans>Add New Designation</Trans>}
         formId={formId}
         loading={loading}
        //  {...props}
        >
            
          <DesignationAddFormComponent 
            onSubmit={handleSubmit}
            id={formId}
            initialValues={initialValuesToSend}
            validationSchema={DesignationAddValidations}
          />
        </FormContainer>
    )
}

export default DesignationAdd
