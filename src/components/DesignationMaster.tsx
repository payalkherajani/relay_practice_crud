import React, { useState } from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { DesignationMaster_designationsprops } from '../__generated__/DesignationMaster_designationsprops.graphql'
import { Layout } from '@saastack/layouts'
import { Trans } from '@lingui/macro'
import DesignationList from './DesignationList'
import { ActionItem } from '@saastack/components/Actions'
import { AddOutlined, EditOutlined } from '@material-ui/icons'
import DesignationAdd from './DesignationAdd'
import DesignationDelete from './DesignationDelete'
import DesignationUpdate from './DesignationUpdate'

interface Props {
    parent: string
    designationsprops: DesignationMaster_designationsprops
    refetch: () => void
}

const DesignationMaster: React.FC<Props> = (props) => {

    const [openAdd, setOpenAdd] = useState(false)
    const [openDelete,setOpenDelete] = useState(false)
    const [selectedId,setSelectedId] = useState('')
    const [openUpdate,setOpenUpdate] = useState(false)

    console.log(props, 'props designation master')

    const handleClick = (id: string, action: 'UPDATE' | 'DELETE') => {
         setSelectedId(id)
          if(action === 'DELETE'){
              setOpenDelete(true)
          }else{
              setOpenUpdate(true)
          }
    }

    const actions: ActionItem[] = [
        {
            icon: AddOutlined,
            title: <Trans>Add</Trans>,
            onClick: () => setOpenAdd(true),
        },
        // {
        //     icon: EditOutlined,
        //     title: <Trans>Edit</Trans>
        // }
    ]

 
    const variablesToPass = { 'parent': props.parent }

    // console.log({variablesToPass})


    const handleAddSuccess = () => {
        setOpenAdd(false)
        props.refetch()
    }

    const handleDeleteSuccess = () => {
         setOpenDelete(false)
         props.refetch()
    }

    const handleUpdateSuccess = () => {
        setOpenUpdate(false)
        props.refetch()
    }

    const list = (
        <DesignationList designationListProps={props.designationsprops.designations.designation} onHandleClick={handleClick} />
    )


    return (
        <Layout header={<Trans>Your Designations</Trans>} col1={list} actions={actions}>
           {
               openAdd && (
                   <DesignationAdd 
                     variables = {variablesToPass}
                     onClose={() => setOpenAdd(false)}
                     onSuccess={handleAddSuccess}
                   />
               )
           }

           {
               openDelete && (
                   <DesignationDelete
                    variables={variablesToPass}
                    id={selectedId}
                    onClose={() => {
                        setOpenDelete(false)
                        setSelectedId('')
                    }}
                    onSuccess={handleDeleteSuccess}
                   
                   
                   />
               )
           }
           {
               openUpdate && (
                   <DesignationUpdate
                     variables={variablesToPass}
                     id={selectedId}
                     onClose={ () => {
                         setOpenUpdate(false)
                         setSelectedId('')
                     }}
                     onSuccess={handleUpdateSuccess}
                     designationUpdateProps={props.designationsprops.designations.designation}
                   
                   />
               )
           }
        </Layout>
    )
}


export default createFragmentContainer(DesignationMaster, {
    designationsprops: graphql`
        fragment DesignationMaster_designationsprops on Query
        @argumentDefinitions(parent: { type: "String" }) {
            designations(parent: $parent) {
                designation {
                    ...DesignationList_designationListProps
                    ...DesignationUpdate_designationUpdateProps
                }
            }
        }
    `,
})
