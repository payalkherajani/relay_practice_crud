import { Trans } from '@lingui/macro'
import { ConfirmContainer } from '@saastack/layouts/containers'
import React, { useState } from 'react'
import { Variables } from 'relay-runtime'
import DeleteDesignationMutation from '../mutations/DeleteDesignationMutation'
import { useRelayEnvironment } from 'react-relay/hooks'
import { useAlert } from '@saastack/core'

interface Props {
    id: string
    variables: Variables
    onClose: () => void
    onSuccess: () => void
}

const DesignationDelete: React.FC<Props> = (props) => {

    const [loading,setLoading] = useState(false)
    const environment = useRelayEnvironment()
    const showAlert = useAlert()

    const handleDelete = () => {
        setLoading(true)
        DeleteDesignationMutation.commit(environment, props.variables, props.id, {
            onSuccess: handleSuccess,
            onError
        })
    }

    const onError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error',
        })
    }
    const handleSuccess = (id: string) => {
        setLoading(false)
        showAlert(<Trans>Designation deleted successfully!</Trans>, {
            variant: 'info',
        })
        props.onSuccess()
    }


    return (
       <ConfirmContainer 
          loading={loading}
          header={<Trans>Delete Designation</Trans>}
          open
          onClose={props.onClose}
          onAction={handleDelete}
       />
    )
}

export default DesignationDelete
