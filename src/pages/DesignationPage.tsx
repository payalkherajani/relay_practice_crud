import React, { useState, useEffect, useRef } from 'react'
import { useConfig } from '@saastack/core'
import { ErrorComponent, Loading } from '@saastack/components'
import { useRelayEnvironment } from 'react-relay/hooks'
import { graphql } from 'react-relay'
import DesignationMaster from '../components/DesignationMaster'
import { fetchQuery } from '@saastack/relay'
import {
    DesignationPageQueryResponse,
    DesignationPageQuery,
} from '../__generated__/DesignationPageQuery.graphql'

interface Props {}

const query = graphql`
    query DesignationPageQuery($parent: String) {
        ...DesignationMaster_designationsprops @arguments(parent: $parent)
    }
`

const DesignationPage: React.FC<Props> = (props) => {

    // console.log(props,"designation page props")
    const { companyId } = useConfig()
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState<any>(null)
    const environment = useRelayEnvironment()
    const dataRef = useRef<DesignationPageQueryResponse | null>(null)

    const fetchDesignations = async () => {
        const variables = {
            parent: companyId,
        }
        try {
            const response = await fetchQuery<DesignationPageQuery>(environment, query, variables, {
                force: true,
            })
            dataRef.current = response
        } catch (error) {
            setError(error)
        } finally {
            setLoading(false)
        }
    }

    useEffect(() => {
        fetchDesignations()
    }, [])

    if (loading) {
        return <Loading />
    }

    if (error) {
        return <ErrorComponent error={error} />
    }

    return (
        <DesignationMaster 
          parent={companyId!}
          designationsprops={dataRef.current!}
          refetch={fetchDesignations}
        />
    )
}

export default DesignationPage
