import { Form, Input, Textarea } from '@saastack/forms'
import React, { useState } from 'react'
import { Trans } from '@lingui/macro'
import { DesignationInput } from '../__generated__/CreateDesignationMutation.graphql'
import { FormProps } from '@saastack/forms/types'
import { Toggle } from '@saastack/components'

interface Props extends FormProps<DesignationInput>{
   update?: boolean
}
const DesignationAddFormComponent: React.FC<Props> = ({ update,...props }) => {

    const [show,setShow] = useState(false)
    
    return (
        <Form { ...props}>
           <Input large name="name" label={<Trans>Title</Trans>} grid={{ xs: 12}} />
           <Toggle
            label={ update? <Trans> Update Description</Trans> : <Trans>Add Description</Trans>}
            show={show}
            onShow={setShow}
           >
           <Textarea grid={{ xs: 12 }} label={<Trans>Description</Trans>} name="description" />

           </Toggle>
        </Form>
    )
}

export default DesignationAddFormComponent
