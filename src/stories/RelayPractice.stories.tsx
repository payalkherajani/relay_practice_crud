import React from 'react'
import DesignationPage  from '../pages/DesignationPage'
import Wrapper from './Wrapper'
// A story captures the rendered state of a UI component. 
//It’s a function that returns a component’s state given a set of arguments.


export default {
    title: 'My-Stories',
    decorators:[
        (storyFunc: () => JSX.Element) => {
            return (
                <Wrapper>{storyFunc()}</Wrapper>
            )
        }
    ]
}

export const DesignationPageStory = () => <DesignationPage />