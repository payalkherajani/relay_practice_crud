import { graphql, commitMutation, Variables } from 'react-relay'
import { MutationCallbacks } from '@saastack/relay'
import { Disposable, Environment } from 'relay-runtime'
import { CreateDesignationInput, CreateDesignationMutation, CreateDesignationMutationResponse, DesignationInput } from '../__generated__/CreateDesignationMutation.graphql'

const mutation = graphql`
    mutation CreateDesignationMutation($input: CreateDesignationInput) {
        createDesignation(input: $input) {
            clientMutationId
            payload {
                id
                name
                description
                roles {
                    id
                    roleName
                    level
                    priority
                    isDefault
                }
            }
        }
    }
`
let tempID = 0

const commit = (
    environment: Environment, 
    variables: Variables, 
    designation:DesignationInput, 
    callbacks?: MutationCallbacks<DesignationInput>
    ): Disposable => {

    const input: CreateDesignationInput = {
        parent: variables.parent,
        designation: { ...designation, description: window.btoa(designation.description!)},
        clientMutationId: `${tempID++}`
    }
    // console.log(input,"input to backend")

   return commitMutation<CreateDesignationMutation>(environment, {
       mutation,
       variables: {
           input 
       },
       onError: (error: Error) => {
        //    console.log(callbacks,"callbacks")
           if(callbacks && callbacks.onError){
            //    console.log(error,"error")                    
            const message = error.message.split('/n')[1]
            callbacks?.onError!(message)
           }
           
       },
       onCompleted: (response: CreateDesignationMutationResponse) => {
           if(callbacks && callbacks.onSuccess){
               callbacks?.onSuccess({ ...designation, ...response?.createDesignation?.payload})
           }
       }
       
   })
}

export  default { commit }