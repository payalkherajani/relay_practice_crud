import { MutationCallbacks } from '@saastack/relay'
import { graphql, commitMutation } from 'react-relay'
import { Disposable, Environment, Variables } from 'relay-runtime'
import { DeleteDesignationInput, DeleteDesignationMutation } from '../__generated__/DeleteDesignationMutation.graphql'

const mutation = graphql`
mutation DeleteDesignationMutation($input: DeleteDesignationInput){
    deleteDesignation(input: $input){
        clientMutationId
    }
}
`

let tempID = 0

const commit = (
    environment: Environment,
    variables: Variables,
    id: string,
    callbacks?: MutationCallbacks<string>
): Disposable => {

    const input: DeleteDesignationInput = {
        id,
        clientMutationId: `${tempID++}`
    }
    return commitMutation<DeleteDesignationMutation>(environment, {
        mutation,
        variables: {
            input
        },
        onError: (error: Error) => {
            if(callbacks && callbacks.onError){
                const message = error.message.split('/n')[1]
                callbacks.onError(message)
            }
        },
        onCompleted: () => {
            if(callbacks && callbacks.onSuccess){
                callbacks.onSuccess(id)
            }
        }
    })

}

export default { commit }